#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.Metaclass')
def module():
    from    abc     import  ABCMeta         as  ABC_Meta
    from    abc     import  abstractmethod  as  abstract_method


    show = 0


    def create_Metaclass_Test(Parent = Type, abstractable = false):
        Metaclass_Test = create_metaclass(
                'Metaclass_Test',

                Parent       = Parent,
                abstractable = abstractable,
            )

        return Metaclass_Test


    def create_TestClass(Metaclass, members = 0):
        name = 'TestClass'

        if members is 0:
            members = {}

        if Metaclass is ABC_Meta:
            TestClass = Metaclass(
                    name,
                    base_classes__Capital_Object,
                    process_object_members(name, members),
                )
        else:
            TestClass = Metaclass.create_class(name, members)

        return TestClass



    def test_abstract_methods():
        if 7:
            #
            #   Hand test to test `produce_find_symbol` with a depth of 5.
            #
            #   Need to make a real unit test for that ...
            #
            class DEF_Meta(ABC_Meta):
                pass

            class XYZ_Meta(ABC_Meta):
                __repr__ = ABC_Meta.__repr__


            Metaclass_Test = create_Metaclass_Test(Parent = XYZ_Meta)
        else:
            Metaclass_Test = create_Metaclass_Test(Parent = ABC_Meta)

        Abstractable_Metaclass_Test = create_Metaclass_Test(Parent = ABC_Meta, abstractable = true)


        @abstract_method
        def xyz():
            pass


        expected_attribute_error = (
                  "`TestClass.operator abstract methods (__abstractmethods__)`:"
                + " class `TestClass` with metaclass `Metaclass_Test` cannot use abstract methods"
            )

        for Metaclass in [ABC_Meta, Metaclass_Test, Abstractable_Metaclass_Test]:
            Test = 0

            with catch_AttributeError(expected_attribute_error) as caught:
                Test = create_TestClass(Metaclass, { 'xyz' : xyz })

            if Metaclass is Metaclass_Test:
                assert Metaclass is Metaclass_Test

                if show:
                    symbol_table = Metaclass.__dict__

                    with indent(arrange('%s:', Metaclass.__name__), prefix = 2):
                        for k in introspect_hidden(Metaclass):
                            if k in symbol_table:
                                v = symbol_table[k]
                            else:
                                v = attribute(Metaclass, k)

                            line('%s: %r', k, v)

                if caught:
                    if show:
                        line('Caught expected error: %s', caught.caught)
                else:
                    raise_runtime_error('did not catch expected error: %s', expected_attribute_error)
            else:
                assert Test.__abstractmethods__ == FrozenSet(['xyz'])
                assert not caught


    def test_class_members():
        Metaclass_Test = create_Metaclass_Test()
        TestClass      = create_TestClass(Metaclass_Test)

        assert type(TestClass.class_basic_size)      is Integer
        assert TestClass.class_bases                 is base_classes__Capital_Object
        assert type(TestClass.class_flags)           is Integer
        assert type(TestClass.class_members)         is MapProxy
        assert TestClass.class_members_offset        is 0
        assert TestClass.class_item_size             is 0
        assert TestClass.class_parent                is Capital_Object
        assert TestClass.class_weak_reference_offset is 0

        for [invalid, use_instead] in ((
                ((  '__base__',             'class_parent'                  )),
                ((  '__bases__',            'class_bases'                   )),
                ((  '__basicsize__',        'class_basic_size'              )),
                ((  '__dict__',             'class_members'                 )),
                ((  '__dictoffset__',       'class_members_offset'          )),
                ((  '__flags__',            'class_flags'                   )),
                ((  '__itemsize__',         'class_item_size'               )),
                ((  '__weakrefoffset__',    'class_weak_reference_offset'   )),
        )):
            if invalid == '__bases__':
                #
                #   `.__bases__` is still permitted and is a synonym for `.class_bases`
                #
                assert TestClass.__bases__ is TestClass.class_bases
                continue

            if invalid == '__dict__':
                #
                #   `.__dict__` is still permitted and is a synonym for `.class_members`
                #
                #   Since this returns a different `MapProxy` every time it is accessed,
                #   we test to make sure the keys are the same
                #
                assert sorted_list(TestClass.__dict__.keys()) == sorted_list(TestClass.class_members.keys())
                continue

            verify_catch_attribute_error(
                    lambda: attribute(TestClass, invalid),
                    arrange("`TestClass.%s`: invalid member `.%s`; use `.%s` instead",
                            invalid,
                            invalid,
                            use_instead),
                )

    def test_immediate_subclasses():
        Metaclass_Test = create_Metaclass_Test()
        TestClass      = create_TestClass(Metaclass_Test)

        Alice = Metaclass_Test.create_class('Alice', Parent = TestClass)
        Bob   = Metaclass_Test.create_class('Bob',   Parent = TestClass)

        verify_catch_attribute_error(
                lambda: TestClass.__subclasses__(),
                (
                      "`TestClass.operator find subclasses (__subclasses__)`:"
                    + " invalid method `.operator find subclasses (__subclasses__)`;"
                    + " use `.class_immediate_subclasses` instead"
                ),
            )

        actual   = sorted_list(m.class_name   for m in TestClass.class_immediate_subclasses())
        expected = ['Alice', 'Bob']

        assert actual == expected


    def test_instance_check():
        Metaclass_Test = create_Metaclass_Test()
        TestClass      = create_TestClass(Metaclass_Test)

        new__TestClass    = Method(new_instance, TestClass)
        create__TestClass = new__TestClass

        instance = create__TestClass()

        assert is_instance(instance, TestClass)       #   Bypasses call to `.__instancecheck__`

        expected_error = (
                  "`TestClass.operator instance check (__instancecheck__)`:"
                + " class `TestClass` cannot check if an instance is of it's class type"
            )

        verify_catch_attribute_error(lambda: is_instance(0, TestClass), expected_error)



    def test_subclass_check():
        #
        #   See NOTE in "UnitTest/Class.py" in function `test_subclass_check__AND__subclass_hook`
        #
        pass


    @share
    def test_metaclass():
        test_abstract_methods()
        test_class_members()
        test_immediate_subclasses()
        test_instance_check()
        test_subclass_check()

        line('PASSED: metaclass')
