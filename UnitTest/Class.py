#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.Class')
def module():
    show = 0


    Python_Abc     = import_module('abc')
    Python_BuiltIn = import_module('__builtin__'  if is_python_2 else   'builtins')
    Python_Pickle  = import_module('pickle')


    #
    #   Types
    #
    ABC_Meta      = Python_Abc.ABCMeta
    PicklingError = Python_Pickle.PicklingError


    #
    #   Functions
    #
    if is_python_2:
        compare = Python_BuiltIn.cmp

    pickle_dump = Python_Pickle.dumps
    pickle_load = Python_Pickle.loads


    def test__portray(t):
        return arrange('<TestClass %r>', t.name)


    def create_Metaclass_Test(Parent = Type, abstractable = false, subclassable = false):
        if abstractable:
            assert Parent is ABC_Meta
        else:
            assert Parent is Type

        Metaclass_Test = create_metaclass(
                'Metaclass_Test',

                Parent        = Parent,
                abstractable  = abstractable,
                subclassable  = subclassable,
            )

        return Metaclass_Test


    def copy_TestClass_from_CapitalObject(ignore_keys = 0, keep_slots = 0, newable = 0):
        Capital_Object__class_members = conjure__class_members(Capital_Object)

        members = {
                '__module__' : 'UnitTest.Shared',
                '__repr__'   : test__portray,
                '__slots__'  : (('name',)),
            }

        if newable:
            members['__new__'] = Capital_Object__operator_new

        provide_member = members.setdefault

        for [k, v] in iterate_items_sorted_by_key(Capital_Object__class_members):
            if (ignore_keys is not 0) and (k in ignore_keys):
                continue

            provide_member(k, v)

        TestClass = Type('TestClass', ((Object,)), members)

        if keep_slots:
            pass
        else:
            del TestClass.__slots__

        return TestClass


    def create_TestClass(Metaclass = Type, newable = false):
        name = 'TestClass'

        members = {
                      '__slots__' : ((
                          'name',                             #   String
                      )),

                      'portray' : test__portray,
                  }

        if Metaclass is Type:
            TestClass = Metaclass(
                    name,
                    base_classes__Capital_Object,
                    process_object_members(name, members, newable = newable),
                )
        else:
            TestClass = Metaclass.create_class(name, members, newable = newable)

        return TestClass


    def create_test_instance(TestClass = 0, make_class = false, newable = false):
        if TestClass is not 0:
            assert make_class is false
            assert newable    is false

        if TestClass is 0:
            TestClass = create_TestClass(newable = newable)

        new__TestClass              = Method(new_instance, TestClass)
        initialize__TestClass__name = TestClass.name.__set__


        def create__TestClass(name):
            t = new__TestClass()

            initialize__TestClass__name(t, name)

            return t


        test_instance = create__TestClass('test')


        if make_class:
            return ((TestClass, test_instance))


        return test_instance



    @share
    def verify_catch_attribute_error(f, expected):
        with catch_AttributeError(expected) as caught:
            f()

        if caught:
            if show:
                line('Caught expected error: %s', caught.caught)
        else:
            raise_runtime_error('did not catch expected error: %s', expected)



    def verify_catch_type_error(f, expected):
        with catch_TypeError(expected) as caught:
            f()

        if caught:
            if show:
                line('Caught expected error: %s', caught.caught)
        else:
            raise_runtime_error('did not catch expected error: %s', expected)



    def test_compare():
        find__Capital_Operator__symbol = produce__find_symbol(Capital_Object)

        operator_mapping = {
                '__cmp__' : 'compare',
                '__eq__'  : '==',
                '__ge__'  : '>=',
                '__gt__'  : '>',
                '__le__'  : '<=',
                '__lt__'  : '<',
                '__ne__'  : '!=',
        }

        for [operator, operator_name] in iterate_items_sorted_by_key(operator_mapping):
            #
            #   Python 3 does not have a `__cmp__` method or a `cmp` function.
            #
            if operator_name == 'compare':
                continue

            expected_attribute_error = arrange(
                      "`TestClass.operator %s (%s)`: instances of type `TestClass` cannot be compared",
                      operator_name,
                      operator,
                )

            ignore_keys = LiquidSet(operator_mapping.keys())
            ignore_keys.remove(operator)

            TestClass = copy_TestClass_from_CapitalObject(ignore_keys)

            test_instance = create_test_instance(TestClass = TestClass)


            #
            #   NOTE:
            #       Need to copy the operator from `Capital_Object`
            #
            #       Only the operator we are testing must exist at our level; otherwise it might
            #       call a different operator.
            #
            #       For example `compare` will call `__eq__`, if the `__eq__` operator exists, it
            #       won't even try to call the `__cmp__` operator.
            #
            set_attribute(TestClass, operator, find__Capital_Operator__symbol(operator))

            if operator == '__cmp__':
                verify_catch_attribute_error(lambda: compare(test_instance, 0), expected_attribute_error)
                continue

            if operator == '__eq__':
                verify_catch_attribute_error(lambda: test_instance == 0, expected_attribute_error)
                continue

            if operator == '__gt__':
                verify_catch_attribute_error(lambda: test_instance > 0, expected_attribute_error)
                continue

            if operator == '__ge__':
                verify_catch_attribute_error(lambda: test_instance >= 0, expected_attribute_error)
                continue

            if operator == '__lt__':
                verify_catch_attribute_error(lambda: test_instance < 0, expected_attribute_error)
                continue

            if operator == '__le__':
                verify_catch_attribute_error(lambda: test_instance <= 0, expected_attribute_error)
                continue

            if operator == '__ne__':
                verify_catch_attribute_error(lambda: test_instance != 0, expected_attribute_error)
                continue

            raise_runtime_error('programming error: operator: %r', operator)

        line('PASSED: test_compare')


    def test__create_class__only_via_the_metaclass__not_via_the_class():
        Metaclass_Test = create_Metaclass_Test()
        TestClass      = create_TestClass(Metaclass_Test)


        def attempt__TestClass__create_class():
            TestClass.create_class()


        verify_catch_attribute_error(
                attempt__TestClass__create_class,
                (
                      "`.create_class` may only be called via the metaclass <Meta_Metaclass Metaclass_Test>;"
                    + " not via the class <Metaclass_Test TestClass>"
                ),
            )


    def test_constructor__and__alter_attribute():
        TestClass = create_TestClass()


        def attempt_create_instance():
            return TestClass('test')


        #<Test 1: operator new (__new__)>
        verify_catch_attribute_error(
                attempt_create_instance,
                (
                      "`TestClass.operator new (__new__)`:"
                    + " instances of type `TestClass` cannot be allocated with `new`"
                ),
            )
        #</Test>

        #<Test 2: constructor (__init__)>
        #
        #   See: https://bugs.python.org/issue5322
        #
        #   We can't delete `.__new__` (in python 3) to restore the default behavior.
        #
        #   Hence we have to create a new TestClass from scratch
        #
        TestClass = create_TestClass(newable = true)

        verify_catch_attribute_error(
                attempt_create_instance,
                (
                      "`TestClass.operator constructor (__init__)`:"
                    + " instances of type `TestClass` cannot be constructed"
                ),
            )
        #</Test>


        #<Test 3: alter attribute>
        def TestClass__constructor(t, name):
            t.name = name


        TestClass.__init__ = TestClass__constructor


        verify_catch_attribute_error(
                attempt_create_instance,
                (
                      "`TestClass.operator set attribute (__setattr__)`:"
                    + " instances of type `TestClass` are immutable and cannot set attributes"
                ),
            )
        #</Test>


        #
        #<Test 4: call to a normal constructor>
        #
        #   Have to restore `__setattr__` so can set the `.name` attribute
        #
        TestClass.__setattr__ = Object__operator__set_attribute

        test_instance = attempt_create_instance()

        assert test_instance.name == 'test'
        #</Test>



    def test_delete_attribute():
        [TestClass, test_instance] = create_test_instance(make_class = 7)


        def attempt_delete_attribute():
            del test_instance.name


        verify_catch_attribute_error(
                attempt_delete_attribute,
                (
                      "`TestClass.operator delete attribute (__delattr__)`:"
                    + " instances of type `TestClass` are immutable and cannot delete attributes"
                ),
            )


        #
        #<Test 4: restore `operator delete attribute`>
        #
        #   Have to restore `__delattr__` so can delete the `.name` attribute
        #
        TestClass.__delattr__ = Object__operator__delete_attribute


        attempt_delete_attribute()

        #
        #   Since `.name` has been deleted, accessing it should generate an attribute error
        #
        verify_catch_attribute_error(
                lambda: test_instance.name,
                'name',
            )
        #</Test>


    def test_format():
        [TestClass, test_instance] = create_test_instance(make_class = true)

        verify_catch_attribute_error(
                lambda: 'test_instance: {}'.format(test_instance),
                (
                      "`TestClass.operator format (__format__)`:"
                    + " instances of type `TestClass` cannot be formatted inside a string (using `format`)"
                ),
            )


        def TestClass__format(t, specification):
            return portray(t)


        TestClass.__format__ = TestClass__format

        expected = "test_instance: <TestClass 'test'>"
        actual   = 'test_instance: {}'.format(test_instance)

        if expected != actual:
            line('expected:  %r', expected)
            line('  actual:  %r', actual)

            raise_runtime_error("`%s` did not return %r (actual: %r)",
                                "'test_instance: {}'.format(test_instance)",
                                expected,
                                actual)


    def test_hash():
        [TestClass, test_instance] = create_test_instance(make_class = true)

        verify_catch_type_error(
                lambda: hash(test_instance),
                arrange("unhashable type: %r", test_instance.__class__.__name__),
            )


        def TestClass__hash(t):
            return 7


        TestClass.__hash__ = TestClass__hash

        expected = 7
        actual   = hash(test_instance)

        if expected != actual:
            line('expected:  %r', expected)
            line('  actual:  %r', actual)

            raise_runtime_error("`%s` did not return %r (actual: %r)",
                                'hash(test_instance)',
                                expected,
                                actual)


    def test_operator_new():
        TestClass = create_TestClass(newable = true)


        def attempt_new():
            return TestClass.__new__(TestClass, 'blue')


        #
        #<Test1: operator new>
        #
        test_instance = attempt_new()


        #
        #   Since `.name` was never initialized, accessing it should generate an attribute error
        #
        verify_catch_attribute_error(
                lambda: test_instance.name,
                'name',
            )
        #</Test>


        #
        #<Test2: Object.__new__>
        #
        #   object.__new__:
        #       python 2: works
        #       python 3: complains `TypeError: object() takes no parameters`
        #                   (Even though we defined our own `.__init__` method)
        #
        def TestClass__constructor(t, name):
            t.name = name

        TestClass.__init__ = TestClass__constructor
        TestClass.__new__  = new_instance

        if is_python_2:
            attempt_new()
        else:
            #show_class_members(TestClass, show_hidden = true)

            verify_catch_type_error(
                    attempt_new,
                    'object() takes no parameters',
                )
        #</Test>


    def test_pickle():
        #
        #   `operator new (__new__)` must be defined to unpickle in python 3.
        #
        TestClass = copy_TestClass_from_CapitalObject(keep_slots = 7, newable = 7)

        test_instance = create_test_instance(TestClass = TestClass)


        def attempt_pickle():
            return pickle_dump(test_instance)


        def catch_PicklingError(message):
            return create_CatchException(PicklingError, message)


        def verify_catch_pickling_error(f, expected):
            with catch_PicklingError(expected) as caught:
                f()

            if caught:
                if show:
                    line('Caught expected error: %s', caught.caught)
            else:
                raise_runtime_error('did not catch expected error: %s', expected)


        #
        #<Test 1: operator reduce extended (__reduce_ex__)>
        #
        verify_catch_attribute_error(
                attempt_pickle,
                (
                      "`TestClass.operator reduce extended (__reduce_ex__)`:"
                    + " instances of type `TestClass` cannot be pickeled"
                )
            )
        #</Test>


        #
        #<Test 2: operator reduce extended (__reduce_ex__)>
        #
        del TestClass.__reduce_ex__

        verify_catch_attribute_error(
                attempt_pickle,
                (
                      "`TestClass.operator reduce (__reduce__)`:"
                    + " instances of type `TestClass` cannot be pickeled"
                )
            )
        #</Test>


        #
        #<Test 3: MISSING operator __getstate__>
        #
        del TestClass.__reduce__


        if is_python_2:
            verify_catch_type_error(
                    attempt_pickle,
                    'a class that defines __slots__ without defining __getstate__ cannot be pickled',
                )
        #</Test>


        #
        #<Test 4: missing in UnitTest.Shared>
        #
        def TestClass__getstate__(t):
            return t.name


        TestClass.__getstate__ = TestClass__getstate__


        if is_python_2:
            expected_error = arrange("Can't pickle %s: it's not found as %s.%s",
                                     TestClass,
                                     TestClass.__module__,
                                     TestClass.__name__)
        else:
            expected_error = arrange("Can't pickle %s: attribute lookup TestClass on %s failed",
                                     TestClass,
                                     TestClass.__module__)

        verify_catch_pickling_error(attempt_pickle, expected_error)
        #</Test>


        #
        #<Test 5: pickle>
        #
        share(
                'TestClass',    TestClass,
            )

        pickled = attempt_pickle()

        del Shared.TestClass
        #</Test>


        #
        #<Test 6: unpickle>
        #
        #   NOTE:
        #       `__setstate__` Must be a `method` when retrieved [as an attribute], so that `t` is auto-bound.
        #
        #       Hence we need the `TestClass__setstate__` wrapper around `initialize__TestClass__name`
        #
        #       That is:
        #           Can't use `TestClass.__setstate__ = initialize__TestClass__name` directly as it
        #           would not bind `self` whe retrieved [as an attribute].
        #
        #           (Would also be super obscure to use `initialize__TestClass__name` directly for
        #           `__setstate__` anyway ...)
        #
        initialize__TestClass__name = TestClass.name.__set__


        def TestClass__setstate__(t, name):
            initialize__TestClass__name(t, name)


        TestClass.__setstate__ = TestClass__setstate__

        share(
                'TestClass',    TestClass,
            )

        result = pickle_load(pickled)

        del Shared.TestClass

        assert result.name == 'test'
        #</Test>


    #
    #   NOTE:
    #       `__subclasscheck__` is part of `Type`.
    #       `__subclasshook__`  is part of `Object`
    #
    #   `ABC_Meta.__subclasscheck__` (derived from `Type`) calls `Object.__subclasshook`.
    #
    #   THUS:
    #
    #       The test for `Type.__subclasscheck__` & `Object.__subclasshook__` are *BOTH* in
    #       this function.
    #
    #   See related NOTE in "UnitTest/Metaclass.py" function `test_subclass_check`
    #
    def test__subclass_check__AND__subclass_hook():
        def attempt__is_subclass():
            assert is_subclass(Object, TestClass) is false


        #
        #<Test 1: cannot call __subclasscheck__>
        #
        Metaclass_Test = create_Metaclass_Test()
        TestClass      = create_TestClass(Metaclass_Test)

        verify_catch_attribute_error(
                attempt__is_subclass,
                (
                      "`TestClass.operator subclass check (__subclasscheck__)`:"
                    + " class `TestClass` cannot check if another class is it's subclass"
                )
            )
        #</Test>

        #
        #<Test 2: Normal class does *NOT* call __subclasshook__>
        #
        Metaclass_Test = create_Metaclass_Test(subclassable = true)
        TestClass      = create_TestClass(Metaclass_Test)

        attempt__is_subclass()
        #</Test>

        #
        #<Test 3: ABC_Meta class does call __subclasshook__>
        #
        Metaclass_Test = create_Metaclass_Test(Parent = ABC_Meta, abstractable = true, subclassable = true)
        TestClass      = create_TestClass(Metaclass_Test)

        verify_catch_attribute_error(
                attempt__is_subclass,
                (
                      "`TestClass.operator sub class hook (__subclasshook__)`:"
                    + " class `TestClass` cannot test for sub class"
                )
            )
        #</Test>


    @share
    def test_class():
        test_compare()
        test__create_class__only_via_the_metaclass__not_via_the_class()
        test_constructor__and__alter_attribute()
        test_delete_attribute()
        test_format()
        test_hash()
        test_operator_new()
        test_pickle()
        test__subclass_check__AND__subclass_hook()

        line('PASSED: class')
