#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.Zone')
def module():
    transport('Capital.Zone',                       'current_zone')


    @share
    def test_zone():
        z = current_zone()

        line('current_zone: %s', z)

        assert z is current_zone()

        line('PASSED: Zone')
