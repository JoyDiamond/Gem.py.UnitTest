#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.Series')
def module():
    require_module('Capital.GarbageCollection')
    require_module('Capital.Series')
    require_module('UnitTest.CacheSupport')


    from Capital import create_series_0, create_series_1, create_series_2, create_series_3, create_series_4
    from Capital import dump_series_statistics, collect_garbage


    #
    #   TestNumber: Specific instances
    #
    eight = conjure_test_number('eight', 8)
    five  = conjure_test_number('five',  5)
    four  = conjure_test_number('four',  4)
    nine  = conjure_test_number('nine',  9)
    one   = conjure_test_number('one',   1)
    seven = conjure_test_number('seven', 7)
    six   = conjure_test_number('six',   6)
    three = conjure_test_number('three', 3)
    two   = conjure_test_number('two',   2)
    zero  = conjure_test_number('zero',  0)


    def test_series_create():
        for loop in [1, 2, 3, 4]:
            s0 = create_series_0()
            s1 = create_series_1(one)
            s2 = create_series_2(one, two)
            s3 = create_series_3(one, two, three)

            assert s0.finish() is (())
            assert s1.finish() == ((one,))
            assert s2.finish() == ((one, two))
            assert s3.finish() == ((one, two, three))

            del s0, s1, s2, s3
            collect_garbage()


    @share
    def test_series():
        test_series_create()

        line('PASSED: series')

        dump_series_statistics()
