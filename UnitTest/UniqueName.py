#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.UniqueName')
def module():
    transport('Capital.UniqueName',                 'create_UniqueName')


    if is_python_2:
        transport('Capital.System',                 'MAXIMUM_INTEGER')


    @share
    def test_unique_name():
        q = create_UniqueName('q')

        assert q.next() == 'q1'
        assert q.next() == 'q2'

        if is_python_2:
            q.skip_value__ALLY__UnitTest(MAXIMUM_INTEGER - 3)

            assert q.next() == 'q' + String(MAXIMUM_INTEGER - 2)
            assert q.next() == 'q' + String(MAXIMUM_INTEGER - 1)
            assert q.next() == 'q' + String(MAXIMUM_INTEGER - 0)
            assert q.next() == 'q' + String(Long(MAXIMUM_INTEGER) + 1)
            assert q.next() == 'q' + String(Long(MAXIMUM_INTEGER) + 2)

        line('PASSED: UniqueName')
