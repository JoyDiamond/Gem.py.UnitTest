#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.Introspection')
def module():
    Python_BuiltIn = __import__('__builtin__'  if is_python_2 else   'builtins')


    python_introspect = Python_BuiltIn.dir


    def create_class_Test2():
        Test = Type('Test', base_classes__Capital_Object, process_object_members('Test', {}))


        class Test2(Test):
            @static_method
            def __repr__():
                return '<Test2>'


        return Test2


    def introspect_old(instance):
        append      = 0
        i           = 0
        find_symbol = 0
        many        = 0

        result = python_introspect(instance)

        for k in result:
            ignore = lookup__hidden_introspection_key(k)

            if ignore is not none:
                if find_symbol is 0:
                    find_symbol = produce__find_symbol(instance)

                if ignore is find_symbol(k):
                    #line('introspect_old: ignore %s', k)

                    if append is 0:
                        many   = result[ : i]
                        append = many.append

                    continue

            if append is not 0:
                append(k)

            i += 1

        if append is 0:
            return Tuple(result)

        return Tuple(many)


    def test_hidden_introspection():
        Test2 = create_class_Test2()

        expected = Tuple(python_introspect(Test2))
        actual   = introspect_hidden(Test2)

        if expected != actual:
            line('actual:   %s', actual)
            line('expected: %s', expected)

            raise_runtime_error('test_hidden_introspection: expected: %r (actual: %r)', expected, actual)


    def test_normal_introspection():
        Test2 = create_class_Test2()

        expected = introspect_old(Test2)
        actual   = introspect(Test2)

        if expected != actual:
            line('actual:   %s', actual)
            line('expected: %s', expected)

            raise_runtime_error('test_normal_introspection: expected: %r (actual: %r)', expected, actual)


    @share
    def test_introspection():
        test_hidden_introspection()
        test_normal_introspection()

        line('PASSED: introspetion')
