#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.Thread')
def module():
    transport('Capital.Thread',                     'thread_identifier')


    @share
    def test_thread():
        #line('thread_identifier: %s', thread_identifier())

        line('PASSED: Thread')
