#
#   Copyright (c) 2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.StringBuilder')
def module():
    transport('Capital.Zone',                       'current_zone')


    def test_reuse():
        z  = current_zone()
        b1 = z.summon_StringBuilder()
        b2 = z.summon_StringBuilder()

        assert b1 is not b2

        line('create_StringBuilder__ALLY__Zone: %s, %s', b1, b2)

        r1 = b1.finish_and_recycle()

        assert r1 is ''

        b1b = z.summon_StringBuilder()

        assert b1 is b1b

        r1 = b1.finish_and_recycle()
        r2 = b2.finish_and_recycle()

        assert r1 is r2 is ''


    def test_write():
        z = current_zone()
        b = z.summon_StringBuilder()

        b.write('hello')
        b.write(' ')
        b.write('world')

        r = b.finish_and_recycle()

        assert r == 'hello world'


    def test_write_2():
        z = current_zone()
        b = z.summon_StringBuilder()

        b.write_2('hello', ' world')

        r = b.finish_and_recycle()

        assert r == 'hello world'


    def test_write_3():
        z = current_zone()
        b = z.summon_StringBuilder()

        b.write_3('hello', ' ', 'world')

        r = b.finish_and_recycle()

        assert r == 'hello world'


    @share
    def test_string_builder():
        test_reuse()
        test_write()
        test_write_2()
        test_write_3()

        line('PASSED: string_builder')
