#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('UnitTest.Sequence')
def module():
    require_module('Capital.Sequence')
    require_module('UnitTest.CacheSupport')


    from Capital import create_sequence_1, create_sequence_2, create_sequence_3, create_sequence_4
    from Capital import create_sequence_many
    from Capital import sequence_0


    #
    #   TestNumber: Specific instances
    #
    eight = conjure_test_number('eight', 8)
    five  = conjure_test_number('five',  5)
    four  = conjure_test_number('four',  4)
    nine  = conjure_test_number('nine',  9)
    one   = conjure_test_number('one',   1)
    seven = conjure_test_number('seven', 7)
    six   = conjure_test_number('six',   6)
    three = conjure_test_number('three', 3)
    two   = conjure_test_number('two',   2)
    zero  = conjure_test_number('zero',  0)


    def test_sequence_create():
        portray_s0 = "{,}"
        portray_s1 = "{" + portray(one) + "}"
        portray_s2 = "{" + portray_s1[1:-1] + ", " + portray(two  ) + "}"
        portray_s3 = "{" + portray_s2[1:-1] + ", " + portray(three) + "}"
        portray_s4 = "{" + portray_s3[1:-1] + ", " + portray(four ) + "}"
        portray_s5 = "{" + portray_s4[1:-1] + ", " + portray(five ) + "}"

        for loop in [1, 2]:
            s0 = sequence_0
            s1 = create_sequence_1(one)
            s2 = create_sequence_2(one, two)
            s3 = create_sequence_3(one, two, three)
            s4 = create_sequence_4(one, two, three, four)
            s5 = create_sequence_many( ((one, two, three, four, five)) )

            assert Tuple(s0) == (())
            assert Tuple(s1) == ((one,))
            assert Tuple(s2) == ((one, two))
            assert Tuple(s3) == ((one, two, three))
            assert Tuple(s4) == ((one, two, three, four))
            assert Tuple(s5) == ((one, two, three, four, five))

            assert portray(s0) == portray_s0
            assert portray(s1) == portray_s1
            assert portray(s2) == portray_s2
            assert portray(s3) == portray_s3
            assert portray(s4) == portray_s4
            assert portray(s5) == portray_s5


    @share
    def test_sequence():
        test_sequence_create()

        line('PASSED: sequence')
